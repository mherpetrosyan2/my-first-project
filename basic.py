single_digits = ['', 'մեկ', 'երկու', 'երեք', 'չորս', 'հինգ', 'վեց', 'յոթ', 'ութ', 'ինը']
nums = [1, 2, 3]

two_digits = {
        10: "տաս",
        11: "տասնմեկ",
        12: "տասներկու",
        13: "տասներեք",
        14: "տասնչորս",
        15: "տասնհինգ",
        16: "տասնվեց",
        17: "տասնյոթ",
        18: "տասնութ",
        19: "տասնինը"
}

tens_multiple = ["", "տասը", "քսան", "երեսուն", "քառասուն", "հիսուն", "վաթսուն", "յոթանասուն", "ութսուն", "իննսուն"]

tens_power = [" հարյուր ", " հազար ", " միլիոն "]


def number_hundred(num: str) -> str:
    """
    The function returns the number in armenian words till 999.
    :param num: string
    :return:The number in words.
    """
    if int(num) == 0:
        return 'զրո'
    elif int(num) < 10:
        return single_digits[int(num)]
    elif int(num) < 100:
        if int(num) in two_digits.keys():
            return two_digits[int(num)]
        if int(num[0]) == 0:
            return tens_multiple[int(num[1])] + single_digits[int(num[2])]
        return tens_multiple[int(num[0])] + single_digits[int(num[1])]
    elif int(num) < 1000:
        two_digit = int(num) % 100
        if two_digit in two_digits.keys():
            return single_digits[int(num[0])] + tens_power[0] + two_digits[int(two_digit)]
        return single_digits[int(num[0])] + tens_power[0] + tens_multiple[int(num[1])] + single_digits[int(num[2])]


def number_to_expression(number: str) -> str:
    """
        The function returns the number in armenian words.
    :param number: string
    :return:The number in words.
    """
    if number[0] == '0' and int(number) != 0:
        return "The number can't start with 0"
    elif len(number) <= 3:
        return number_hundred(number)
    elif len(number) <= 6:
        num_1 = number[-3:]
        num_2 = number[-6:-3]
        return number_hundred(num_2) + tens_power[1] + number_hundred(num_1)
    elif len(number) <= 9:
        num_1 = number[-3:]
        num_2 = number[-6:-3]
        num_3 = number[-9:-6]
        num_in_word = number_hundred(num_3) + tens_power[2] + number_hundred(num_2) + tens_power[1]\
            + number_hundred(num_1)
        num_with_no_thousands = number_hundred(num_3) + tens_power[2] + number_hundred(num_1)
        if num_2 == '000':
            return f'{number}: {num_with_no_thousands}'
        return f'{number}: {num_in_word}'


n = input('Enter some number: ')
print(number_to_expression(n))

